/*
 * _SPI.c
 *
 * Created: 05/02/2019 00:32:08
 *  Author: Oki
 */ 
#include "_SPI.h"
#include "_PINS.h"
#include <avr/interrupt.h>
void SPI_Initialize_As_Master(void)
{
	_CSoutPut();
	_CSPinHigh();
	DDRB |= (1<<PINB0) | (1<<PINB1) | (1<<PINB2); //set SS, SCK and MOSI to be ouptups
	SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR0); // Enabke the SPI ENABLE, ENABLE IT AS MASTER AND THEN SET THGE CLOCK FREQUENCY TO BE fclk/16
	SPCR &=~(1<<CPOL) | (1<<CPHA); //Falling Shift SCK-EDGE | CAPTURE SCK-Edge Rising edge
	spiInfoReg.allSPIInfo = SPSR; //clear spi interrupt
	spiInfoReg.allSPIInfo = SPDR;
	spiInfoReg.allSPIInfo = 0;
	spiInfoReg.clearToSend = 1;
}
void SPI_Master_Send(char *ptr)
{
	_CSPinLow();
	if(spiInfoReg.clearToSend == 1)
	{
		ptr_to_strchar = ptr;
		if(*ptr!= 0)
		{
			SPDR = *ptr_to_strchar;
		}
		else
		{
			_CSPinHigh();
			spiInfoReg.clearToSend = 1;
		}
	}
}
unsigned char SPI_Master_Read_Char(void)
{
	unsigned char temp;
	temp = SPDR;
	return temp;
}
void SPI_Read()
{
	*receivedSpIDataBuffer = SPI_Master_Read_Char();
	receivedSpIDataBuffer++;
}
ISR(SPI_STC_vect)
{
	ptr_to_strchar++;
	if (*ptr_to_strchar != 0)
	{
		SPDR = *ptr_to_strchar;
		
	} 
	else
	{
		_CSPinHigh();
		spiInfoReg.clearToSend = 1;
	}
	SPI_Read();
}