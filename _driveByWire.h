/*
 * _driveByWire.h
 *
 * Created: 30/01/2019 18:00:27
 *  Author: Oki
 */ 


#ifndef _DRIVEBYWIRE_H_
#define _DRIVEBYWIRE_H_
#define leftPedalAddress 0x4D
#define rightPedalAddress 0x4C
#define leftPedalMinimum 0x29
#define rightPedalMinimum 0x52
unsigned char leftPedalValue;
unsigned char rightPedalValue;
void initializeADC(void);
void initializeTimerAndExternalInterrupt(void);
void driveLeftPedal(unsigned char leftPedal);
void driveRightPedal(unsigned char rightPedal);
void speedLimitDriveByWire(void);
#endif /* _DRIVEBYWIRE_H_ */