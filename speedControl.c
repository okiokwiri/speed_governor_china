/*
 * speedControl.c
 *
 * Created: 14/02/2019 09:56:38
 *  Author: Oki
 */ 
#include "_driveByWire.h"
#include "SIM868.h"
#include "_PINS.h"
#include "speedControl.h"
#include "TWI.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void normalDrive(void)
{
	driveLeftPedal(leftPedalValue);
	driveRightPedal(rightPedalValue);
}
void overSpeedMeasures(void)
{
	driveLeftPedal(0xFF);
	driveRightPedal(0xFF);
}
void initializeAll(void)
{
	setLedsToBeOutputs();
	initializeADC();
	initializeTimerAndExternalInterrupt();
	TWI_Master_Initialize();
	sei();
	ADCSRA |= (1<<ADSC); //start conversion ADC
	//initializeSIM868();
}
void callibrateGovernor(void)
{
	while (callibrated == 0)
	{
		
		obtainNavigationData();
		callibrationSpeed = obtainGNSSSpeed();
		if (callibrationSpeed > 85)
		{
			overSpeedMeasures();
		}
		else
		{
			normalDrive();
		}
		if((callibrationSpeed>5)&&(callibrationSpeed<12))
		{
			speedGradient_1 = speedSensorValue / callibrationSpeed;
			callibrationCounter++;
		}
		if ((callibrationSpeed>14)&&(callibrationSpeed<22))
		{
			speedGradient_2 = speedSensorValue/callibrationSpeed;
			callibrationCounter++;
		}
		if((callibrationSpeed>28))
		{
			speedGradient_3 = speedSensorValue/callibrationSpeed;
			callibrationCounter++;
			
		}
		if (callibrationCounter == 3)
		{
			speedSensorGradient = (speedGradient_1 + speedGradient_2 + speedGradient_3) / 3;
			callibrated = 1;
			turnGreenLedOn();
			break;
		}
	}
}
void checkIfSpeeding()
{
	if((speedSensorValue/32)>85)
	{
		overSpeedMeasures();
	}
	else
	{
		normalDrive();
	}
}

