/*
 * SIM868.h
 *
 * Created: 14/02/2019 09:03:36
 *  Author: Oki
 */ 


#ifndef SIM868_H_
#define SIM868_H_
int setupGNSSPosition ;
int settingUpGNSS ;
int setUpGSSComplete ;
char setUpGNSSResponse[5];
int gnssSpeed;
char *latitude;
char *longitude;
void initializeSIM868(void);
void setUpGNSS(void);
void obtainGNSSInformation(void);
#endif /* SIM868_H_ */