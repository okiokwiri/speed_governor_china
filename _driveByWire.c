/*
 * _driveByWire.c
 *
 * Created: 30/01/2019 18:18:19
 *  Author: Oki
 */ 
#include "_driveByWire.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include "TWI.h"
unsigned char messageBuf[4];
void initializeADC(void)
{
	 //ADMUX |= (1 << REFS0) | (1 << ADLAR); //LEFT SHIFT RESULT. AREF FOR REFERENCE
	 ADMUX = 0x60;
	 ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0) | (1 << ADIE)|(1<<ADEN); //prescaler X-TAL/128 ENABLE ADC INTEERUPT
	 
	 
}
void initializeTimerAndExternalInterrupt(void)
{
	EICRB |= (1 << ISC41) | (1 << ISC40); // enable interrupt on the rising edge of INT4
	EIMSK |= (1 << INT4); //enable the external interrupt
	TCCR1B |= (1 << WGM12); //wave generation mode two
	TCCR1B |= (1 << CS12) | (1 << CS10); //set prescaler to 1024
	TIMSK1 |= (1 << TOIE1); //enable timer overflow interrupt
	TCNT1 = 49911;
}
void driveLeftPedal(unsigned char leftPedal) 
{

	messageBuf[0] = (leftPedalAddress<<TWI_ADR_BITS) | (FALSE<<TWI_READ_BIT); // The first byte must always consists of General Call code or the TWI slave address.
	messageBuf[1] = (leftPedal & 0x0F);
	messageBuf[2] = ((leftPedal & 0x0F) << 4);
    TWI_Start_Transceiver_with_Data( messageBuf, 3 );
}
void driveRightPedal(unsigned char rightPedal)
{
	messageBuf[0] = (rightPedalAddress<<TWI_ADR_BITS) | (FALSE<<TWI_READ_BIT);
	messageBuf[1] = (rightPedal & 0x0F);
	messageBuf[2] = ((rightPedal & 0x0F) << 4);
	TWI_Start_Transceiver_with_Data(messageBuf , 3);
}
ISR(ADC_vect)
{
	//rightPedalValue = ADCH;
	switch (ADMUX)
	{
		case 0x60:
		leftPedalValue = ADCH;
		ADMUX = 0x61;
		break;
		case 0x61:
		rightPedalValue = ADCH;
		//uart_3_write("lk\n");
		ADMUX = 0x60;
		break;
		default:
		break;
	}
	ADCSRA |= (1<<ADSC);
}