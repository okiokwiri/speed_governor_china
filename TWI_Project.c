/*
 * TWI_Project.c
 *
 * Created: 30/01/2019 00:24:49
 *  Author: Oki
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "speedControl.h"
#include "_PINS.h"
#ifndef F_CPU
#define F_CPU 16000000UL
#endif
int initialized =0;
// Sample TWI transmission commands

int main(void)
{
	initializeAll();
	//callibrateGovernor();
	//uart_3_init(9600);
    while(1)
    {
		//checkIfSpeeding();  
		//normalDrive(); 
		//overSpeedMeasures();
		//normalDrive();
		//turnOnBuzzer();
		obtainGNSSInformation();
		_delay_ms(500);
	
	}
}
ISR(TIMER1_OVF_vect)
{
	speedSensorValue = speedCounter;
	speedCounter = 0;
}
ISR(INT4_vect)
{
	speedCounter++;
}
ISR(USART3_RX_vect)
{
	uart_3_read();
}

