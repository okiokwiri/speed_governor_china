/*
 * TWI.c
 *
 * Created: 30/01/2019 01:02:28
 *  Author: Oki
 */ 
#include "TWI.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
static unsigned char TWI_buf[ TWI_BUFFER_SIZE ];    // Transceiver buffer
static unsigned char TWI_msgSize;                   // Number of bytes to be transmitted.
unsigned char TWI_Transceiver_Busy(void)
{
	return (TWCR & (1<<TWIE)); //if the TWI interrupt is enabled then the transceiver is busy
}
void TWI_Master_Initialize(void)
{
	TWSR = 0; //No prescalers
	TWBR = 12;
	TWDR = 0xFF;                                      // Default content = SDA released.
	TWCR = (1<<TWEN)|                                 // Enable TWI-interface and release TWI pins.
	(0<<TWIE)|(0<<TWINT)|                      // Disable Interrupt.
	(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|           // No Signal requests.
	(0<<TWWC);                                 //                         //
}
unsigned char TWI_Get_State_Info(void)
{
	while(TWI_Transceiver_Busy()); 
	return(TWI_STATUS); // Wait until TWI has completed the transmission and then return the error status code
	
}	
void TWI_Start_Transceiver_with_Data(unsigned char *msg, unsigned char msgSize)
{
	unsigned char temp;
	//next we check if the transceiver is busy 
	while(TWI_Transceiver_Busy());
	TWI_msgSize = msgSize; //number of data bytes to transmit
	TWI_buf[0] = msg[0];  //Store Slave Address with R/W setting
	if (!(msg[0] & (1<<TWI_READ_BIT))) // If the transmission is intended to be a read operation
	{
		for (temp = 1; temp< msgSize; temp++)
		{
			TWI_buf[temp] = msg[temp];
		}
	} 
	i2cStatusReg.all = 0;
	TWCR = (1<<TWEN)|                             // TWI Interface enabled.
	(1<<TWIE)|(1<<TWINT)|                  // Enable TWI Interrupt and clear the flag.
	(0<<TWEA)|(1<<TWSTA)|(0<<TWSTO)|       // Initiate a START condition.
	(0<<TWWC);                             //
}
/****************************************************************************
Call this function to resend the last message. The driver will reuse the data previously put in the transceiver buffers.
The function will hold execution (loop) until the TWI_ISR has completed with the previous operation,
then initialize the next operation and return.
****************************************************************************/
void TWI_Start_Transceiver(void)
{
	while(TWI_Transceiver_Busy());
	i2cStatusReg.all=0;
	TWCR = (1<<TWEN)|                             // TWI Interface enabled.
	(1<<TWIE)|(1<<TWINT)|                  // Enable TWI Interrupt and clear the flag.
	(0<<TWEA)|(1<<TWSTA)|(0<<TWSTO)|       // Initiate a START condition.
	(0<<TWWC);
}
/****************************************************************************
Call this function to read out the requested data from the TWI transceiver buffer. I.e. first call
TWI_Start_Transceiver to send a request for data to the slave. Then Run this function to collect the
data when they have arrived. Include a pointer to where to place the data and the number of bytes
requested (including the address field) in the function call. The function will hold execution (loop)
until the TWI_ISR has completed with the previous operation, before reading out the data and returning.
If there was an error in the previous transmission the function will return the TWI error code.
****************************************************************************/
unsigned char TWI_Get_Data_From_Transceiver(unsigned char *message, unsigned char messageSize)
{
	unsigned char i;
	while(TWI_Transceiver_Busy()); // Wait till TWI is ready for the next transmission
	if (i2cStatusReg.transmissionSuccesful)
	{
		for (i = 0; i<messageSize;i++)
		{
			message[i] = TWI_buf[i];
		}
	}
	return(i2cStatusReg.transmissionSuccesful);
}
// ********** Interrupt Handlers ********** //
/****************************************************************************
This function is the Interrupt Service Routine (ISR), and called when the TWI interrupt is triggered;
that is whenever a TWI event has occurred. This function should not be called directly from the main
application.
****************************************************************************/
ISR(TWI_vect)
{
	static unsigned char TWI_bufPtr;
	
	switch (TWI_STATUS)
	{
	case TWI_START_SENT: //START has been transmitted
	case TWI_REP_START_SENT: //Repeated START has been transmitted
		TWI_bufPtr = 0;
	case TWI_MT_SLAW_ACK: //SLA+W has been transmitted and ACK received
	case TWI_MT_DATA_ACK: //Data byte has been transmitted and ACK received	
		if (TWI_bufPtr < TWI_msgSize)
		{
			TWDR = TWI_buf[TWI_bufPtr++];
			TWCR = (1<<TWEN)|                                 // TWI Interface enabled
			(1<<TWIE)|(1<<TWINT)|                      // Enable TWI Interrupt and clear the flag to send byte
			(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|           //
			(0<<TWWC);
		}
		else
		{
			i2cStatusReg.transmissionSuccesful = TRUE;
			 TWCR = (1<<TWEN)|                                 // TWI Interface enabled
			 (0<<TWIE)|(1<<TWINT)|                      // Disable TWI Interrupt and clear the flag
			 (0<<TWEA)|(0<<TWSTA)|(1<<TWSTO)|           // Initiate a STOP condition.
			 (0<<TWWC);                                 //
			
		}
		break;
		case TWI_MR_DATA_ACK: //Data byte has been received and ACK transmitted
			TWI_buf[TWI_bufPtr++]=TWDR;
		case TWI_MR_SLAR_ACK: //SLA+R has been transmitted and ACK received
			if (TWI_bufPtr < TWI_msgSize - 1)
			{
				TWCR = (1<<TWEN)|                                 // TWI Interface enabled
				(1<<TWIE)|(1<<TWINT)|                      // Enable TWI Interrupt and clear the flag to read next byte
				(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|           // Send ACK after reception
				(0<<TWWC);
			} 
			else //Send NACK after Reception
			{
				 TWCR = (1<<TWEN)|                                 // TWI Interface enabled
				 (1<<TWIE)|(1<<TWINT)|                      // Enable TWI Interrupt and clear the flag to read next byte
				 (0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|           // Send NACK after reception
				 (0<<TWWC);
			}
			case TWI_MT_DATA_NACK:           // SLA+W has been transmitted and NACK received
			case TWI_MR_SLAR_NACK:			// SLA+R has been transmitted and NACK received    
			case TWI_MR_DATA_NACK:			 // Data byte has been transmitted and NACK received
			case TWI_BUS_ERROR:
			default:
			//i2cStatusReg = TWI_STATUS;
			
			// Reset TWI Interface
			TWCR = (1<<TWEN)|                                 // Enable TWI-interface and release TWI pins
			(0<<TWIE)|(0<<TWINT)|                      // Disable Interrupt
			(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|           // No Signal requests
			(0<<TWWC);                                 //		
	}	
}