/*
 * speedControl.h
 *
 * Created: 14/02/2019 09:54:36
 *  Author: Oki
 */ 


#ifndef SPEEDCONTROL_H_
#define SPEEDCONTROL_H_


int callibrated;
int speedSensorGradient;
int speedGradient_1;
int speedGradient_2;
int speedGradient_3 ;
int callibrationCounter;
int callibrationSpeed;
int speedSensorValue;
int speedCounter;
void callibrateGovernor(void);
void normalDrive(void);
void overSpeedMeasures(void);
void initializeAll(void);
void checkIfSpeeding(void);
#endif /* SPEEDCONTROL_H_ */