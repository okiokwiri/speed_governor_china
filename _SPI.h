/*
 * _SPI.h
 *
 * Created: 30/01/2019 22:51:35
 *  Author: Oki
 */ 


#ifndef _SPI_H_
#define _SPI_H_
#define _TRUE	1
#define _FALSE	0
#define _CSPort PORTC
#define _CSDataDirection DDRC
#define _CSPin PINH4
#define _CSoutPut() (_CSDataDirection |= (1<<_CSPin))
#define _CSPinHigh() (_CSPort |= (1<<_CSPin))
#define _CSPinLow() (_CSPort &=~(1<<_CSPin))

char*    ptr_to_strchar;
char* receivedSpIDataBuffer;
union SPI_Info
{
	unsigned char allSPIInfo;
	struct  
	{
		unsigned char clearToSend:1;
		unsigned char spiMasterRead_Write:1;
	};
	}spiInfoReg;
void SPI_Initialize_As_Master(void);
void SPI_Master_Send(char* ptr);
unsigned char SPI_Master_Read_Char(void);
void SPI_Read(void);





#endif /* _SPI_H_ */