/*
 * SIM868.c
 *
 * Created: 14/02/2019 09:06:26
 *  Author: Oki
 */ 
#include <stdio.h>
#include <stdlib.h>
#include "SIM868.h"
#include "_uart.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

void setUpGNSS(void)
{
	if(!settingUpGNSS)
	{
		settingUpGNSS=1;
		if (setupGNSSPosition == 0)
		{
			uart_3_write("AT\r\n");
			strcpy(setUpGNSSResponse,"OK");
			
		} 
		else if (setupGNSSPosition == 1)
		{
			uart_3_write("AT+CGNSPWR=1\r\n");
			strcpy(setUpGNSSResponse,"OK");
		}
		else if (setupGNSSPosition == 2)
		{
			 uart_3_write("AT+CGNSSEQ=");
			 uart_3_print_char(34);
			 uart_3_write("RMC");
			 uart_3_print_char(34);
			 uart_3_print_char(13);
			 uart_3_write("\n");
			 strcpy(setUpGNSSResponse,"OK");
		}
		else
		{
			setUpGSSComplete = 1;
			settingUpGNSS = 1;
		}
			
	}
}
void initializeSIM868(void)
{

	uart_3_init(9600);

	while(!setUpGSSComplete)
	{
		setUpGNSS();
		if (strstr(uart_3_buffer,setUpGNSSResponse))
		{
			
			uart_3_clear_buffer();
			setupGNSSPosition++;
			settingUpGNSS = 0;
		}
		else
		{
			//uart_3_clear_buffer();
			_delay_ms(500);
			//uart_3_write(uart_3_buffer);
			settingUpGNSS = 0;
		}
	}
}
void obtainNavigationData(void)
{
	uart_3_clear_buffer();
	uart_3_write("AT+CGNSINF\r\n");
	_delay_ms(100);
}
void obtainGNSSInformation(void)
{
	uart_3_write("AT+CGNSINF\r\n");
	if (strstr(uart_3_buffer,"+CGNSINF"))
	{
		 char *pch = strtok(uart_3_buffer, ",");
		 int i = 0;
		 while (pch != NULL)
		 {
			 if(i==4)
			 {
				 strcpy(latitude,pch);
			 }
			 if(i==5)
			 {
				 strcpy(longitude,pch);
			 }
			 if (i == 6)
			 {
				// uart_3_write(pch);
				 gnssSpeed = atoi(pch);
			 }
			 i++;
			 pch=strtok(NULL,",");
		 }
		 uart_3_clear_buffer();
		 uart_3_write("latitude");
		 uart_3_write("	");
		 uart_3_write(latitude);
		 uart_3_write("\n");
		 uart_3_write("longitude");
		 uart_3_write("	");
		 uart_3_write(longitude);
		 uart_3_write("\n");
		
	}
	else
	{
		_delay_ms(500);
	}
	
	
}
