/*
 * TWI.h
 *
 * Created: 30/01/2019 00:26:02
 *  Author: Oki
 */ 


#ifndef TWI_H_
#define TWI_H_
/****************************************************************************
  TWI Status/Control register definitions
****************************************************************************/
#define TWI_BUFFER_SIZE 4   // Set this to the largest message size that will be sent including address byte.

#define TWI_TWBR            0x0C         // TWI Bit rate Register setting.
                                        // Se Application note for detailed 
                                        // information on setting this value.

/****************************************************************************
  Bit and byte definitions
****************************************************************************/
#define TWI_READ_BIT  0       // Bit position for R/W bit in "address byte".
#define TWI_ADR_BITS  1       // Bit position for LSB of the slave address bits in the init byte.

#define TRUE          1
#define FALSE         0


/****************************************************************************
  Global definitions
****************************************************************************/
#define TWI_STATUS (TWSR & 0xF8)
// TWI Status Codes
#define TWI_START_SENT			0x08 // Start sent
#define TWI_REP_START_SENT		0x10 // Repeated Start sent
// Master Transmitter Mode
#define TWI_MT_SLAW_ACK			0x18 // SLA+W sent and ACK received
#define TWI_MT_SLAW_NACK		0x20 // SLA+W sent and NACK received
#define TWI_MT_DATA_ACK			0x28 // DATA sent and ACK received
#define TWI_MT_DATA_NACK		0x30 // DATA sent and NACK received
// Master Receiver Mode
#define TWI_MR_SLAR_ACK			0x40 // SLA+R sent, ACK received
#define TWI_MR_SLAR_NACK		0x48 // SLA+R sent, NACK received
#define TWI_MR_DATA_ACK			0x50 // Data received, ACK returned
#define TWI_MR_DATA_NACK		0x58 // Data received, NACK returned
#define TWI_BUS_ERROR			0x00
#define TWISendStart()		(TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN)|(1<<TWIE)) // Send the START signal, enable interrupts and TWI, clear TWINT flag to resume transfer.
#define TWISendStop()		(TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN)|(1<<TWIE)) // Send the STOP signal, enable interrupts and TWI, clear TWINT flag.
#define TWISendTransmit()	(TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE)) // Used to resume a transfer, clear TWINT and ensure that TWI and interrupts are enabled.
#define TWISendACK()		(TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE)|(1<<TWEA)) // FOR MR mode. Resume a transfer, ensure that TWI and interrupts are enabled and respond with an ACK if the device is addressed as a slave or after it receives a byte.
#define TWISendNACK()		(TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWIE)) // FOR MR mode. Resume a transfer, ensure that TWI and interrupts are enabled but DO NOT respond with an ACK if the device is addressed as a slave or after it receives a byte.
#define F_CPU 16000000UL
// Function declarations
#define setSCLFrequency() (TWBR = 12)
// Miscellaneous States
#define TWI_LOST_ARBIT			0x38 // Arbitration has been lost
#define TWI_NO_RELEVANT_INFO	0xF8 // No relevant information available
#define TWI_ILLEGAL_START_STOP	0x00 // Illegal START or STOP condition has been detected
#define TWI_SUCCESS				0xFF // Successful transfer, this state is impossible from TWSR as bit2 is 0 and read only
union TWI_Status_Reg
{
	unsigned char all;
	struct
	{
		unsigned char transmissionSuccesful:1;
		unsigned char unusedBits:7;
	};
}i2cStatusReg;


void TWI_Master_Initialize(void);
void TWI_Start_Transceiver_with_Data(unsigned char *msg, unsigned char msgSize);
void TWI_Start_Transceiver(void);
unsigned char TWI_Transceiver_Busy(void);
unsigned char TWI_Get_State_Info(void);
unsigned char TWI_Get_Data_From_Transceiver(unsigned char *message, unsigned char messageSize);

//TWIInfoStruct TWIInfo;
#endif /* TWI_H_ */