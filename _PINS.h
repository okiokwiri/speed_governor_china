/*
 * _PINS.h
 *
 * Created: 30/01/2019 17:54:55
 *  Author: Oki
 */ 


#ifndef _PINS_H_
#define _PINS_H_
#define relayPort PORTK
#define relayPin PINK3
#define relayDataDirection DDRK
#define buzzerPin PINB5
#define buzzerPort PORTB
#define buzzerDataDirection DDRB
#define ledDataDirection DDRE
#define ledPort PORTE
#define redLedPin PINE2
#define greenLedPin PINE3
#define blueLedPin PINE6
#define setLedsToBeOutputs() (ledDataDirection |= (1<<redLedPin)|(1<<greenLedPin)|(1<<blueLedPin))
#define buzzerToBeOutput() (buzzerDataDirection |= (1<<buzzerPin))
#define turnOnBuzzer() (buzzerPort |= (1<<buzzerPin))
#define turnOffBuzzer() (buzzerPort &=~ (1<<buzzerPin))
#define setRelaysToBeOutputs() ( relayDataDirection|= (1<<relayPin))
#define turnRedLedOn()(ledPort |= (1<<redLedPin))
#define turnRedLedOff()(ledPort &=~ (1<<redLedPin))
#define turnGreenLedOn()(ledPort |= (1<<greenLedPin))
#define turnGreenLedOff()(ledPort &=~ (1<<greenLedPin))
#define turnBlueLedOn()(ledPort |= (1<<blueLedPin))
#define turnBlueLedOff()(ledPort &=~ (1<<blueLedPin))
#define turnRelayOff()(relayPort |= (1<<relayPin))
#define turnRelayOn()(relayPort &=~ (1<<relayPin))
#endif /* _PINS_H_ */